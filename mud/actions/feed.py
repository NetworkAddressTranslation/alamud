# -*- coding: utf-8 -*-
# Copyright (C) 2018 Etienne Fouquet
#==============================================================================

from .action import Action2, Action3
from mud.events import FeedEvent, FeedWithEvent


class FeedAction(Action2):
    EVENT = FeedEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "feed"


class FeedWithAction(Action3):
    EVENT = FeedWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "feed-with"
